package game

import (
	p "brooks/lawrence/ttt/player"
	"fmt"
	"strconv"
	"strings"
)

// TicTacToe struct reprenting a Tic-Tac-Toe game
type TicTacToe struct {
	board  []p.Player
	size   int
	player p.Player
}

// NewGame creates a new Tic-Tac-Toe game
func NewGame(size int) *TicTacToe {
	return &TicTacToe{
		board:  make([]p.Player, size*size),
		size:   size,
		player: -1,
	}
}

func (t *TicTacToe) columnWon(x int) bool {
	sum := 0
	for col := x % t.size; col < len(t.board); col = col + t.size {
		sum = sum + int(t.board[col])
	}
	if sum == t.size || sum == -t.size {
		return true
	}
	return false
}

func (t *TicTacToe) rowWon(y int) bool {
	nextRow := (y + 1) * t.size

	sum := 0
	for rowIdx := y * t.size; rowIdx < nextRow; rowIdx++ {
		sum = sum + int(t.board[rowIdx])
	}
	if sum == t.size || sum == -t.size {
		return true
	}
	return false
}

func (t *TicTacToe) downRightWon(x, y int) bool {
	if x != y {
		return false
	}

	sum := 0
	for i := 0; i < len(t.board); i = i + t.size + 1 {
		sum = sum + int(t.board[i])
	}
	if sum == t.size || sum == -t.size {
		return true
	}
	return false
}

func (t *TicTacToe) upLeftWon(x, y int) bool {
	if x+y != (t.size - 1) {
		return false
	}

	sum := 0
	for i := t.size - 1; i < len(t.board); i = i + t.size - 1 {
		sum = sum + int(t.board[i])
	}
	if sum == t.size || sum == -t.size {
		return true
	}
	return false
}

func (t *TicTacToe) coordsToIndex(x, y int) int {
	return (y * t.size) + x
}

func (t *TicTacToe) validMoveIndex(idx int) bool {
	return t.board[idx] == 0
}

func (t *TicTacToe) validMoveCoords(x, y int) bool {
	return t.validMoveIndex(t.coordsToIndex(x, y))
}

func (t *TicTacToe) boardToString() string {
	var bldr strings.Builder
	bldr.WriteString(" ")
	for i := 0; i < t.size; i++ {
		bldr.WriteString("│")
		bldr.WriteString(strconv.Itoa(i))
	}
	bldr.WriteString("│\n")

	for y := 0; y < t.size; y++ {
		bldr.WriteString("─╆")
		for i := 0; i < t.size-1; i++ {
			bldr.WriteString("━╈")
		}
		bldr.WriteString("━┪\n")

		bldr.WriteString(strconv.Itoa(y))
		for x := 0; x < t.size; x++ {
			bldr.WriteString("┃")
			bldr.WriteString(fmt.Sprint(t.board[(y*t.size)+x]))
		}
		bldr.WriteString("┃\n")
	}

	bldr.WriteString("─┺")
	for i := 0; i < t.size-1; i++ {
		bldr.WriteString("━┻")
	}
	bldr.WriteString("━┛\n")

	return bldr.String()
}

func (t *TicTacToe) String() string {
	return fmt.Sprint(t.boardToString())
}

func (t *TicTacToe) moveWins(x, y int) bool {
	if t.columnWon(x) {
		return true
	}

	if t.rowWon(y) {
		return true
	}

	if t.downRightWon(x, y) {
		return true
	}

	if t.upLeftWon(x, y) {
		return true
	}

	return false
}

// Play current player takes coords
func (t *TicTacToe) Play(x, y int) (bool, error) {
	if !t.validMoveCoords(x, y) {
		return false, fmt.Errorf("position (%d,%d) is already claimed", x, y)
	}

	t.board[t.coordsToIndex(x, y)] = t.player

	if t.moveWins(x, y) {
		return true, nil
	}

	t.player = t.player * -1

	return false, nil
}
