package player

// Player represents the player
type Player int

func (p Player) String() string {
	if p < 0 {
		return "X"
	}
	if p > 0 {
		return "O"
	}
	return " "
}
